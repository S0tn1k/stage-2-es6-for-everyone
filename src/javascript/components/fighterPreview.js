import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterBoxElement = createElement({ tagName: 'div', className: 'fighter-preview_box' });
    const fighterImage = createFighterImage(fighter);
    const fighterName = createElement({ tagName: 'p', className: `fighter-preview___name` });
    const fighterHealth = createElement({ tagName: 'p', className: `fighter-preview___info` });
    const fighterAttack = createElement({ tagName: 'p', className: `fighter-preview___info` });
    const fighterDefense = createElement({ tagName: 'p', className: `fighter-preview___info` });

    fighterName.textContent = fighter.name;
    fighterHealth.textContent = `health :` + fighter.health;
    fighterAttack.textContent = `attack :` + fighter.attack;
    fighterDefense.textContent = `defense :` + fighter.defense;

    fighterBoxElement.append(fighterName);
    fighterBoxElement.append(fighterHealth);
    fighterBoxElement.append(fighterAttack);
    fighterBoxElement.append(fighterDefense);

    fighterBoxElement.append(fighterImage);

    fighterElement.append(fighterBoxElement);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
