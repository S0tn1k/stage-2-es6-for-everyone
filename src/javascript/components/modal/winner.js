import { createElement } from '../../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  showModal({
    title: fighter.name,
    bodyElement: createElement({ tagName: 'img', attributes: { src: fighter.source } }),
  });
}
