import { controls } from '../../constants/controls';
import { getRandomArbitrary } from '../helpers/random';
import { updateHealthIndicator } from './arena';

export async function fight(firstFighter, secondFighter) {
  const firstFighterState = {
    health: firstFighter.health,
    isBlocking: false,
    usedCriticalHitAt: 0,
  };
  const secondFighterState = {
    health: secondFighter.health,
    isBlocking: false,
    usedCriticalHitAt: 0,
  };

  const lastKeysPressed = [];
  const updateKeysPressed = (key) => {
    if ((lastKeysPressed.length = 3)) {
      lastKeysPressed.splice(0, 1);
    }

    lastKeysPressed.push(key);
  };

  return new Promise((resolve) => {
    document.addEventListener('keydown', ({ code }) => {
      if (code === controls['PlayerOneBlock']) firstFighterState.isBlocking = true;
      else if (code === controls['PlayerTwoBlock']) secondFighterState.isBlocking = true;

      updateKeysPressed(code);

      if (isPressedPlayerOneCriticalHitCombination(lastKeysPressed) && canUseCriticalHit(firstFighterState)) {
        secondFighterState.health -= getCriticalDamage(firstFighter);
        if (secondFighterState.health < 0) secondFighterState.health = 0;

        updateHealthIndicator('right', (100 * secondFighterState.health) / secondFighter.health);
        firstFighterState.usedCriticalHitAt = Date.now();
      }

      if (isPressedPlayerTwoCriticalHitCombination(lastKeysPressed) && canUseCriticalHit(secondFighterState)) {
        firstFighterState.health -= getCriticalDamage(secondFighter);
        if (firstFighterState.health < 0) firstFighterState.health = 0;

        updateHealthIndicator('left', (100 * firstFighterState.health) / firstFighter.health);
        secondFighterState.usedCriticalHitAt = Date.now();
      }

      if (code === controls['PlayerOneAttack']) {
        if (firstFighterState.isBlocking) return;
        if (secondFighterState.isBlocking) return;

        secondFighterState.health -= getDamage(firstFighter, secondFighter);
        if (secondFighterState.health < 0) secondFighterState.health = 0;

        updateHealthIndicator('right', (100 * secondFighterState.health) / secondFighter.health);
      } else if (code === controls['PlayerTwoAttack']) {
        if (secondFighterState.isBlocking) return;
        if (firstFighterState.isBlocking) return;

        firstFighterState.health -= getDamage(secondFighter, firstFighter);
        if (firstFighterState.health < 0) firstFighterState.health = 0;

        updateHealthIndicator('left', (100 * firstFighterState.health) / firstFighter.health);
      }

      if (!firstFighterState.health || !secondFighterState.health)
        resolve(!firstFighterState.health ? secondFighter : firstFighter);
    });

    document.addEventListener('keyup', (e) => {
      if (e.code === controls['PlayerOneBlock']) firstFighterState.isBlocking = false;
      else if (e.code === controls['PlayerTwoBlock']) secondFighterState.isBlocking = false;
    });
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getCriticalDamage(attacker) {
  return attacker.attack * 2;
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandomArbitrary(1, 2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomArbitrary(1, 2);
  return fighter.defense * dodgeChance;
}

function isPressedPlayerOneCriticalHitCombination(keysPressed) {
  return controls['PlayerOneCriticalHitCombination'].every((key) => keysPressed.includes(key));
}

function isPressedPlayerTwoCriticalHitCombination(keysPressed) {
  return controls['PlayerTwoCriticalHitCombination'].every((key) => keysPressed.includes(key));
}

function canUseCriticalHit(fighterState) {
  return Date.now() - fighterState.usedCriticalHitAt > 10000 && !fighterState.isBlocking;
}
